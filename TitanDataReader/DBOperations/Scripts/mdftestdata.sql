CREATE TABLE mdftestdata (
	testdataid varchar(256),
	configurationid varchar(256),
	time	timestamp	NOT NULL,
	channel1	double precision	not null,
	channel2	double precision	not null,
	channel3	double precision	not null,
	channel4	double precision	not null,
	channel5	double precision	not null,
	channel6	double precision	not null,
	channel7	double precision	not null,
	channel8	double precision	not null,
	channel9	double precision	not null,
	channel10	double precision	not null
);

SELECT create_hypertable('mdftestdata', 'time');