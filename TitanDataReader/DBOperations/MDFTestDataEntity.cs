﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitanDataReader.DBOperations
{
    public class mdftestdataEntity
    {
        public string testdataid { get; set; }
        public string configurationid { get; set; }
        public DateTime timestamp { get; set; }
        public double channel1 { get; set; }
        public double channel2 { get; set; }
        public double channel3 { get; set; }
        public double channel4 { get; set; }
        public double channel5 { get; set; }
        public double channel6 { get; set; }
        public double channel7 { get; set; }
        public double channel8 { get; set; }
        public double channel9 { get; set; }
        public double channel10 { get; set; }
    }
}
