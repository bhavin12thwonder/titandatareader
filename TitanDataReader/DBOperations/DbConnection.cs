﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Configuration;

namespace TitanDataReader.DBOperations
{
    public static class DbConnection
    {
        public static NpgsqlConnection con;
        public static NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
        }

        public static void InsertRecord(string query)
        {
            using (NpgsqlConnection con = GetConnection())
            {                
                NpgsqlCommand cmd = new NpgsqlCommand(query, con);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
