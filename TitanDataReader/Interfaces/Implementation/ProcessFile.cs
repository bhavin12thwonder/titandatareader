﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TitanDataReader.Interfaces.Interface;

namespace TitanDataReader.Interfaces.Implementation
{
    public class ProcessFile : IProcessFile
    {
        public Dictionary<double, double> CreateDictionary(double[] channeldata, double[] time)
        {
            Dictionary<double, double> timeChnValueDict = new Dictionary<double, double>();
            for (int x = 0; x < time.Length; x++)
            {
                timeChnValueDict.Add(time[x], channeldata[x]);
            }
            return timeChnValueDict;
        }
    }
}
