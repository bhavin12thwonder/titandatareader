﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitanDataReader.Interfaces.Interface
{
    public interface IProcessFile
    {
        Dictionary<double, double> CreateDictionary(double[] channeldata, double[] time);
    }
}
