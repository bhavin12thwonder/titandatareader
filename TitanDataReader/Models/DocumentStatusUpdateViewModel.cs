﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitanDataReader.Models
{
    public class DocumentStatusUpdateViewModel
    {
        public Guid TestDataId { get; set; }
        public int Status { get; set; }
        public Guid UserModifiedById { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
