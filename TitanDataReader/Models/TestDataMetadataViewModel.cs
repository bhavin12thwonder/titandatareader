﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitanDataReader.Models
{
    public class TestDataMetadataViewModel
    {
        public Guid Id { get; set; }
        public string OtherProperties { get; set; }
        public List<ChannelGroupMetadataViewModel> ChannelGroups { get; set; }

    }

    public class ChannelGroupMetadataViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public string MetadataId { get; set; }
        public List<ChannelMetadataInsertViewModel> Channels { get; set; }

    }

    public class ChannelMetadataInsertViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MetadataId { get; set; }
        public string ChannelGroupMetadataId { get; set; }
        public string ChannelGroupName { get; set; }
        public string Unit { get; set; }
        public string ExtraJsonProperties { get; set; }
    }
}
