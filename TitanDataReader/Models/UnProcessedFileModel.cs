﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitanDataReader.Models
{
    public class UnProcessedFileModel
    {
        public Guid Id { get; set; }
        public int CurrentStatus { get; set; }
        public string FilePath { get; set; }
        public List<MeasurementAnalyticsConfigurationChannelViewModel> Configuration { get; set; }
    }

    public class UnProcessedFileModelBaseModel
    {
        public List<UnProcessedFileModel> result { get; set; }
    }
}
