﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TitanDataReader.Models
{
    public class ChannelDataViewModel
    {
        public string ChannelName { get; set; }
        public double[] xAxisLabel { get; set; }
        public double[] yAxisLabel { get; set; }
    }
}
