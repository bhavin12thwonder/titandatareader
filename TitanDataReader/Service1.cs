﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.ServiceProcess;
using System.Configuration;
using System.Threading;
using TitanDataReader.Models;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.IO;
using jnsoft.Helpers;
using jnsoft.MDF.V4;
using TitanDataReader.DBOperations;
using TitanDataReader.Interfaces.Implementation;
using jnsoft.MDF;

namespace TitanDataReader
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        int ScheduleTime = Convert.ToInt32(ConfigurationManager.AppSettings["ThreadTime"]);
        string TitanBaseUrl = ConfigurationManager.AppSettings["TitanBaseUrl"];
        string TitanAuthUrl = ConfigurationManager.AppSettings["TitanAuthUrl"];
        public Thread Worker = null;
        public Thread Worker2 = null;
        Queue<UnProcessedFileModel> queue = new Queue<UnProcessedFileModel>();
        ProcessFile ps = new ProcessFile();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                ThreadStart start = new ThreadStart(GetUnProcessedFiles);
                Worker = new Thread(start);
                Worker.Start();

                ThreadStart start2 = new ThreadStart(ProcessFile);
                Worker2 = new Thread(start2);
                Worker2.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnStop()
        {
            try
            {
                if ((Worker != null) && Worker.IsAlive)
                {
                    Worker.Abort();
                }
                if ((Worker2 != null) && Worker2.IsAlive)
                {
                    Worker2.Abort();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void GetUnProcessedFiles()
        {
            while (true)
            {
                while (!queue.Any())
                {
                    // Call titan api if queue is empty and get files to process
                    var GetUnprocessedDataApi = TitanBaseUrl + "api/TestRequestDocumentTestData/GetUnprocessedData?extension=mf4";
                    var token = GetTitanAccessToken();

                    WebRequest request = WebRequest.Create(GetUnprocessedDataApi);
                    // Add headers
                    request.Headers.Add("Authorization", "Bearer " + token.access_token);
                    request.Headers.Add("TenantId", "76B0B8B2-EDCC-4292-90A5-8A0EE6B06BEC");
                    request.Headers.Add("FingerPrint", "fingerprint");
                    request.ContentType = "application/json";

                    // Get the response.  
                    WebResponse response = request.GetResponse();
                    var responseFromServer = string.Empty;
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        // Open the stream using a StreamReader for easy access.  
                        StreamReader reader = new StreamReader(dataStream);
                        // Read the content.  
                        responseFromServer = reader.ReadToEnd();
                    }
                    // Close the response.  
                    response.Close();

                    // Convert response
                    var responseJson = JsonConvert.DeserializeObject<UnProcessedFileModelBaseModel>(responseFromServer);

                    // Add files to queue
                    foreach (var file in responseJson.result)
                    {
                        queue.Enqueue(file);
                    }
                }
                Thread.Sleep(ScheduleTime * 60 * 1000);
            }

        }

        public void ProcessFile()
        {
            while (true)
            {
                // Check if queue has items, dequeue items from queue to process
                if (queue.Count != 0)
                {
                    while (queue.Any())
                    {
                        var unProcessedFile = queue.Dequeue();
                        // Access file and read the file 
                        //Mdf4File file = Mdf4File.Open(unProcessedFile.FilePath);

                        var file = MDFReader.open(unProcessedFile.FilePath);

                        var FileCreationDt = File.GetLastWriteTime(unProcessedFile.FilePath).Date.ToString("MM/dd/yyyy");

                        // Send the meta data back to titan api
                        TestDataMetadataViewModel vm = new TestDataMetadataViewModel();
                        vm.Id = unProcessedFile.Id;
                        vm.ChannelGroups = new List<ChannelGroupMetadataViewModel>();

                        List<ChannelGroupMetadataViewModel> channelGroupList = new List<ChannelGroupMetadataViewModel>();

                        foreach (var chnGrp in file.OpenChannelGroups)
                        {
                            ChannelGroupMetadataViewModel chnGrpVm = new ChannelGroupMetadataViewModel();
                            chnGrpVm.Name = ((CGBLOCKV4)chnGrp).AcquisitionName;
                            chnGrpVm.MetadataId = ((CGBLOCKV4)chnGrp).AcquisitionName;
                            chnGrpVm.Channels = new List<ChannelMetadataInsertViewModel>();
                            List<ChannelMetadataInsertViewModel> channelList = new List<ChannelMetadataInsertViewModel>();

                            //var masterChannelData = Mdf4Sampler.LoadFull(chnGrp.MasterChannel).FirstOrDefault().GetSpan<double>();
                            //var channelsWOMasterChannel = chnGrp.Channels.Where(x => x.Name != chnGrp.MasterChannel.Name).ToList();
                            var channelsWOMasterChannel = chnGrp.CNBlocks.Where(x => x.ChannelType != ChannelType.Master).ToList();
                            foreach (var chn in channelsWOMasterChannel)
                            {
                                ChannelMetadataInsertViewModel chnInsert = new ChannelMetadataInsertViewModel()
                                {
                                    Name = chn.Name,
                                    Unit = chn.Unit,
                                    MetadataId = chn.Name + "_" + ((CGBLOCKV4)chn.CGBlock).AcquisitionName,
                                    ChannelGroupName = ((CGBLOCKV4)chn.CGBlock).AcquisitionName,
                                    ChannelGroupMetadataId = ((CGBLOCKV4)chn.CGBlock).AcquisitionName
                                };
                                channelList.Add(chnInsert);

                                // Put channel data in time series database
                                //var samples = Mdf4Sampler.LoadFull(chn).FirstOrDefault().GetSpan<double>();
                                //for (int i = 0; i < masterChannelData.Length; i++)
                                //{
                                //    var time = string.Format("{0:00}:{1:00}:{2:00.000000}", (int)masterChannelData[i] / 3600, (int)(masterChannelData[i] % 3600) / 60, (double)masterChannelData[i] % 60);

                                //    string query = "INSERT INTO public.mdftestdata(testdataid, time, channelgroupname, channelname, value, unit)" +
                                //                   " VALUES('" + unProcessedFile.Id + "','" + FileCreationDt + " " + time + "','" + chnGrp.Name + "','"+ chn.Name + "'," + samples[i] + ",'" + chn.Unit + "')";
                                //    DbConnection.InsertRecord(query);
                                //}
                            }

                            chnGrpVm.Channels.AddRange(channelList);
                            channelGroupList.Add(chnGrpVm);
                        }
                        vm.ChannelGroups.AddRange(channelGroupList);

                        //Send metadat to Titan
                        SendMetadataToTitan(vm);

                        //Save data in timeseries
                        SaveTimeSeriesData(file, FileCreationDt, unProcessedFile.Id, unProcessedFile.Configuration);

                        // Close file
                        file.Dispose();


                    }
                }
                else
                {
                    Thread.Sleep(ScheduleTime * 60 * 1000);
                }
            }

        }

        public void SendMetadataToTitan(TestDataMetadataViewModel viewModel)
        {
            var responseFromServer = string.Empty;
            var GetUnprocessedDataApi = TitanBaseUrl + "api/TestRequestDocumentTestData/SaveProcessedMetadata";
            var token = GetTitanAccessToken();

            WebRequest request = WebRequest.Create(GetUnprocessedDataApi);
            // Add headers
            request.Headers.Add("Authorization", "Bearer " + token.access_token);
            request.Headers.Add("TenantId", "76B0B8B2-EDCC-4292-90A5-8A0EE6B06BEC");
            request.Headers.Add("FingerPrint", "fingerprint");
            request.ContentType = "application/json";
            request.Method = "POST";
            request.Credentials = CredentialCache.DefaultCredentials;

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(viewModel);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var response = (HttpWebResponse)request.GetResponse();

            using (Stream dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                responseFromServer = reader.ReadToEnd();
            }
            // Close the response.  
            response.Close();
        }

        public void SaveTimeSeriesData(MDFReader mf4, string FileCreationDt, Guid testdataid, List<MeasurementAnalyticsConfigurationChannelViewModel> titanConfiguration)
        {
            // Comment below lines once we have actual configuration from TITAN
            #region static data

            var configuration = new List<MeasurementAnalyticsConfigurationChannelViewModel>();
            foreach(var config in titanConfiguration)
            {
                configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = config.Name, Position = config.Position });
            }
            configuration = configuration.OrderBy(x => x.Position).ToList();

            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN1 message ID=0x110:VICM_NMCmd_Battery", Position = 1 });
            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN2 message ID=0x245:BPCM_Pack_IsoRes", Position = 2 });
            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN2 message ID=0x245:BPCM_Pack_IsoRes_V", Position = 3 });
            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN2 message ID=0x245:BPCM_FC_IsoRes", Position = 4 });
            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN2 message ID=0x245:BPCM_FC_IsoRes_V", Position = 5 });
            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN2 message ID=0x245:BPCM_Aux_IsoRes", Position = 6 });
            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN2 message ID=0x245:BPCM_Aux_IsoRes_V", Position = 7 });
            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN2 message ID=0x245:BPCM_Link_IsoRes", Position = 8 });
            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN2 message ID=0x245:BPCM_Link_IsoRes_V", Position = 9 });
            //configuration.Add(new MeasurementAnalyticsConfigurationChannelViewModel { Name = "from CAN2 message ID=0x64F:BPCM_info_mux", Position = 10 });

            #endregion

            var list = new List<ICNBLOCK>();

            foreach (var grp in configuration)
            {
                var channelGroupChannelName = grp.Name.Split(':');
                var chnGrpName = channelGroupChannelName[0];
                var chnName = channelGroupChannelName[1];

                list.Add(mf4.OpenChannels.Where(x => x.Name == chnName && (((CGBLOCKV4)x.CGBlock).AcquisitionName) == chnGrpName).FirstOrDefault());
            }


            List<double[]> time = new List<double[]>();
            List<double[]> arrayList = new List<double[]>();
            List<Dictionary<double, double>> timeChnValueDict = new List<Dictionary<double, double>>();

            foreach (var chn in list)
            {
                //var channelData = Mdf4Sampler.LoadFull(chn, chn.Master);
                double[][] channelData;
                var channelIdex = chn.CGBlock.CNBlocks.FindIndex(x => x.Name == chn.Name);
                var timeIdex = chn.CGBlock.CNBlocks.FindIndex(x => x.ChannelType == ChannelType.Master);
                if (!mf4.getData(chn.CGBlock, out channelData, ValueObjectFormat.Physical, double.MinValue, double.MaxValue))
                {
                    // failed to read data
                    //return -2;
                }
                var chndata = channelData[channelIdex];
                arrayList.Add(chndata);
                var timedata = channelData[timeIdex]; //time will always be first 
                time.Add(timedata);
                //Dictionary<double, double> timeChnValue = new Dictionary<double, double>();
                //for (int x = 0; x < timedata.Length; x++)
                //{
                //    timeChnValue.Add(timedata[x], chndata[x]);
                //}
                //timeChnValueDict.Add(timeChnValue);

                timeChnValueDict.Add(ps.CreateDictionary(chndata, timedata));

                var timeOrdered = time[0].ToList();
                for (int z = 1; z < timeChnValueDict.Count; z++)
                {
                    timeOrdered.Union(time[z]);
                }
                timeOrdered = timeOrdered.OrderBy(a => a).ToList();

                double[] lastFoundItems = new double[list.Count];
                int dictCount = 0;
                foreach (var dict in timeChnValueDict)
                {
                    lastFoundItems[dictCount] = dict.Values.First();
                    dictCount++;
                }

                for (int i = 0; i < timeOrdered.Count; i++)
                {
                    int count = 0;
                    // Fixed array of 25
                    double[] chnValues = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
                    foreach (var chnData in timeChnValueDict)
                    {
                        double? value = null;
                        if (chnData.ContainsKey(timeOrdered[i]))
                        {
                            value = chnData[timeOrdered[i]];
                            lastFoundItems[count] = value.Value;
                        }
                        else
                        {
                            value = lastFoundItems[count];
                        }
                        chnValues[count] = value.Value;
                        count++;
                    }

                    var timeVal = string.Format("{0:00}:{1:00}:{2:00.000000}", (int)timeOrdered[i] / 3600, (int)(timeOrdered[i] % 3600) / 60, (double)timeOrdered[i] % 60);

                    string query = "INSERT INTO public.mdftestdata(testdataid, time, channel1, channel2, channel3, " +
                                                                   "channel4, channel5, channel6, channel7, channel8, channel9, channel10)" +
                                   "VALUES('" + testdataid + "','" + FileCreationDt + " " + timeVal + "'," + chnValues[0] + "," + chnValues[1] + "," + chnValues[2] + "," +
                                              + chnValues[3] + "," + chnValues[4] + "," + chnValues[5] + chnValues[6] + "," + chnValues[7] + "," + chnValues[8] + "," + chnValues[9] + ")";
                    DbConnection.InsertRecord(query);
                }
            }

            var insertChannelNameQuery = "INSERT INTO public.mdfmetadata(testdataid";
            // Save channel name in timescale db
            for (int i = 0; i < list.Count; i++)
            {
                insertChannelNameQuery += ",channelName" + (i + 1);
            }
            insertChannelNameQuery += ") VALUES('" + testdataid + "'";
            foreach (var chn in list)
            {
                insertChannelNameQuery += ",'" + chn.Name + "'";
            }
            insertChannelNameQuery += ")";
            DbConnection.InsertRecord(insertChannelNameQuery);

            mf4.Dispose();
            // Update process status in Titan
            UpdateFileProcessStatus(testdataid);
        }

        private void UpdateFileProcessStatus(Guid testdataid)
        {
            var responseFromServer = string.Empty;
            var GetUnprocessedDataApi = TitanBaseUrl + "api/TestRequestDocumentTestData/UpdateStatus";
            var token = GetTitanAccessToken();

            WebRequest request = WebRequest.Create(GetUnprocessedDataApi);
            // Add headers
            request.Headers.Add("Authorization", "Bearer " + token.access_token);
            request.Headers.Add("TenantId", "76B0B8B2-EDCC-4292-90A5-8A0EE6B06BEC");
            request.Headers.Add("FingerPrint", "fingerprint");
            request.ContentType = "application/json";
            request.Method = "POST";
            request.Credentials = CredentialCache.DefaultCredentials;

            var viewmodel = new DocumentStatusUpdateViewModel {
                TestDataId = testdataid,
                Status = 1
            };

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(viewmodel);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var response = (HttpWebResponse)request.GetResponse();

            using (Stream dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                responseFromServer = reader.ReadToEnd();
            }
            // Close the response.  
            response.Close();
        }

        private TitanAccessToken GetTitanAccessToken()
        {
            var responseFromServer = string.Empty;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("Username", ConfigurationManager.AppSettings["Username"]);
            parameters.Add("Password", ConfigurationManager.AppSettings["Password"]);
            parameters.Add("grant_type", "password");
            parameters.Add("Fingerprint", "fingerprint");

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.PostAsync(TitanAuthUrl, new FormUrlEncodedContent(parameters)).Result;
                responseFromServer = response.Content.ReadAsStringAsync().Result;
            }

            var tokenObj = JsonConvert.DeserializeObject<TitanAccessToken>(responseFromServer);

            return tokenObj;
        }

    }
}
